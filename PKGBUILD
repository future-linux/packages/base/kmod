# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=kmod
pkgver=30
pkgrel=1
pkgdesc="Linux kernel module management tools and library"
arch=('x86_64')
url='https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git'
license=('GPL2')
groups=('base')
depends=('glibc' 'xz' 'zlib')
makedepends=('bash' 'binutils' 'bison' 'coreutils' 'flex' 'gcc'
    'gettext' 'gzip' 'make' 'openssl' 'pkg-config' 'sed')
source=(https://www.kernel.org/pub/linux/utils/kernel/${pkgname}/${pkgname}-${pkgver}.tar.xz)
sha256sums=(f897dd72698dc6ac1ef03255cd0a5734ad932318e4adbaebc7338ef2f5202f9f)

build() {
    cd ${pkgname}-${pkgver}

    ${configure}           \
        --sysconfdir=/etc  \
        --with-openssl     \
        --with-zstd        \
        --with-zlib

    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install

    install -vdm755 ${pkgdir}/usr/sbin
    for target in depmod insmod modinfo modprobe rmmod; do
        ln -sfv /usr/bin/kmod ${pkgdir}/usr/sbin/$target
    done

    ln -sfv kmod ${pkgdir}/usr/bin/lsmod
}
